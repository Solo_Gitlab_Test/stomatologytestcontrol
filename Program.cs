﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Windows.Forms;

namespace StomatologyTestControl
{
  public static class Program
  {
    public static String strNotCorrectQuestions = @"NotCorrectQuestions.txt";

    public static Int32 QuestionsPart01ControlSum = 6;
    public static Int32 QuestionsPart02ControlSum = 21;
    public static Int32 QuestionsPart03ControlSum = 27;
    public static Int32 QuestionsPart04ControlSum = 12;
    public static Int32 QuestionsPart05ControlSum = 32;
    public static Int32 QuestionsPart06ControlSum = 31; // без 11-го вопроса
    public static Int32 QuestionsPart07ControlSum = 6;
    public static Int32 QuestionsPart08ControlSum = 3;
    public static Int32 QuestionsPart09ControlSum = 24;
    public static Int32 QuestionsPart10ControlSum = 36;
    public static Int32 QuestionsPart11ControlSum = 16;
    public static Int32 QuestionsPart12ControlSum = 10;
    public static Int32 QuestionsPart13ControlSum = 100;
    public static Int32 QuestionsPart14ControlSum = 73;
    public static Int32 QuestionsPart15ControlSum = 44;
    public static Int32 QuestionsPart16ControlSum = 49;
    public static Int32 QuestionsPart17ControlSum = 40;

    public static String QuestionsPart01Name = "01. Общие вопросы анатомии, гистологии, физиологии органов полости рта";
    public static String QuestionsPart02Name = "02. Клинические, лабораторные и инструментальные методы исследования в стоматологии";
    public static String QuestionsPart03Name = "03. Материаловедение. Лекарствоведение. Обезболивание в стоматологии";
    public static String QuestionsPart04Name = "04. Профилактика стоматологических заболеваний. Реабилитация пациентов в стоматологии";
    public static String QuestionsPart05Name = "05. Терапевтическая стоматология";
    public static String QuestionsPart06Name = "06. Хирургическая стоматология (без 11-го вопроса)";
    public static String QuestionsPart07Name = "07. Ортопедическая стоматология";
    public static String QuestionsPart08Name = "08. Стоматологическая помощь детям";
    public static String QuestionsPart09Name = "09. Система и политика здравоохранения в РФ";
    public static String QuestionsPart10Name = "10. Теоретические основы сестринского дела в стоматологии";
    public static String QuestionsPart11Name = "11. Этические и психологические аспекты деятельности медицинской сестры стоматологического отделения (кабинета)";
    public static String QuestionsPart12Name = "12. Медицина катастроф";
    public static String QuestionsPart13Name = "13. Неотложная помощь";
    public static String QuestionsPart14Name = "14. Основные виды манипуляций в сестринском деле";
    public static String QuestionsPart15Name = "15. Инфекционная безопасность пациентов и персонала. Инфекционный контроль в стоматологии. Дезинфекционное дело";
    public static String QuestionsPart16Name = "16. Инфекционная безопасность пациентов и медперсонала.Инфекционный контроль в стоматологии. ВИЧ-инфекция";
    public static String QuestionsPart17Name = "17. Правовые аспекты в деятельности медицинской сестры";

    public static String QuestionsPart01FileName = @"TestQuestionsData\01. Вопросы - Общие вопросы анатомии, гистологии, физиологии органов полости рта.txt";
    public static String QuestionsPart02FileName = @"TestQuestionsData\02. Вопросы - Клинические, лабораторные и инструментальные методы исследования в стоматологии.txt";
    public static String QuestionsPart03FileName = @"TestQuestionsData\03. Вопросы - Материаловедение. Лекарствоведение. Обезболивание в стоматологии.txt";
    public static String QuestionsPart04FileName = @"TestQuestionsData\04. Вопросы - Профилактика стоматологических заболеваний. Реабилитация пациентов в стоматологии.txt";
    public static String QuestionsPart05FileName = @"TestQuestionsData\05. Вопросы - Терапевтическая стоматология.txt";
    public static String QuestionsPart06FileName = @"TestQuestionsData\06. Вопросы - Хирургическая стоматология (без 11-го вопроса).txt";
    public static String QuestionsPart07FileName = @"TestQuestionsData\07. Вопросы - Ортопедическая стоматология.txt";
    public static String QuestionsPart08FileName = @"TestQuestionsData\08. Вопросы - Стоматологическая помощь детям.txt";
    public static String QuestionsPart09FileName = @"TestQuestionsData\09. Вопросы - Система и политика здравоохранения в РФ.txt";
    public static String QuestionsPart10FileName = @"TestQuestionsData\10. Вопросы - Теоретические основы сестринского дела в стоматологии.txt";
    public static String QuestionsPart11FileName = @"TestQuestionsData\11. Вопросы - Этические и психологические аспекты деятельности медицинской сестры стоматологического отделения (кабинета).txt";
    public static String QuestionsPart12FileName = @"TestQuestionsData\12. Вопросы - Медицина катастроф.txt";
    public static String QuestionsPart13FileName = @"TestQuestionsData\13. Вопросы - Неотложная помощь.txt";
    public static String QuestionsPart14FileName = @"TestQuestionsData\14. Вопросы - Основные виды манипуляций в сестринском деле.txt";
    public static String QuestionsPart15FileName = @"TestQuestionsData\15. Вопросы - Инфекционная безопасность пациентов и персонала. Инфекционный контроль в стоматологии. Дезинфекционное дело.txt";
    public static String QuestionsPart16FileName = @"TestQuestionsData\16. Вопросы - Инфекционная безопасность пациентов и медперсонала.Инфекционный контроль в стоматологии. ВИЧ-инфекция.txt";
    public static String QuestionsPart17FileName = @"TestQuestionsData\17. Вопросы - Правовые аспекты в деятельности медицинской сестры.txt";

    public static String AnswersPart01FileName = @"TestAnswersData\01. Ответы - Общие вопросы анатомии, гистологии, физиологии органов полости рта.txt";
    public static String AnswersPart02FileName = @"TestAnswersData\02. Ответы - Клинические, лабораторные и инструментальные методы исследования в стоматологии.txt";
    public static String AnswersPart03FileName = @"TestAnswersData\03. Ответы - Материаловедение. Лекарствоведение. Обезболивание в стоматологии.txt";
    public static String AnswersPart04FileName = @"TestAnswersData\04. Ответы - Профилактика стоматологических заболеваний. Реабилитация пациентов в стоматологии.txt";
    public static String AnswersPart05FileName = @"TestAnswersData\05. Ответы - Терапевтическая стоматология.txt";
    public static String AnswersPart06FileName = @"TestAnswersData\06. Ответы - Хирургическая стоматология (без 11-го вопроса).txt";
    public static String AnswersPart07FileName = @"TestAnswersData\07. Ответы - Ортопедическая стоматология.txt";
    public static String AnswersPart08FileName = @"TestAnswersData\08. Ответы - Стоматологическая помощь детям.txt";
    public static String AnswersPart09FileName = @"TestAnswersData\09. Ответы - Система и политика здравоохранения в РФ.txt";
    public static String AnswersPart10FileName = @"TestAnswersData\10. Ответы - Теоретические основы сестринского дела в стоматологии.txt";
    public static String AnswersPart11FileName = @"TestAnswersData\11. Ответы - Этические и психологические аспекты деятельности медицинской сестры стоматологического отделения (кабинета).txt";
    public static String AnswersPart12FileName = @"TestAnswersData\12. Ответы - Медицина катастроф.txt";
    public static String AnswersPart13FileName = @"TestAnswersData\13. Ответы - Неотложная помощь.txt";
    public static String AnswersPart14FileName = @"TestAnswersData\14. Ответы - Основные виды манипуляций в сестринском деле.txt";
    public static String AnswersPart15FileName = @"TestAnswersData\15. Ответы - Инфекционная безопасность пациентов и персонала. Инфекционный контроль в стоматологии. Дезинфекционное дело.txt";
    public static String AnswersPart16FileName = @"TestAnswersData\16. Ответы - Инфекционная безопасность пациентов и медперсонала.Инфекционный контроль в стоматологии. ВИЧ-инфекция.txt";
    public static String AnswersPart17FileName = @"TestAnswersData\17. Ответы - Правовые аспекты в деятельности медицинской сестры.txt";

    public static void ShowErrorMessage(String strErrorMessage)
    {
      DialogResult rslt = MessageBox.Show(strErrorMessage, "Error", MessageBoxButtons.OK);
    }

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new frm_Main());
    }
  }
}
