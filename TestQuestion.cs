﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace StomatologyTestControl
{
  public class TestQuestion
  {
    private Int32   m_iQuestionNumber                = 0;
    private String  m_strQuestionText                = String.Empty;
    private String  m_strQuestionsPartName           = String.Empty;
    private Int32   m_iQuestionAnswer                = 0;
    
    private Enums.QuestionStatus m_enmQuestionStatus = Enums.QuestionStatus.Neutral;

    public TestQuestion()
    { 
    }

    public Int32 QuestionNumber
    {
      get
      {
        return m_iQuestionNumber;
      }
      set
      {
        m_iQuestionNumber = value;
      }
    }

    public String QuestionText
    {
      get
      {
        return m_strQuestionText;
      }
      set
      {
        m_strQuestionText = value;
      }
    }

    public String QuestionsPartName
    {
      get
      {
        return m_strQuestionsPartName;
      }
      set
      {
        m_strQuestionsPartName = value;
      }
    }

    public Int32 QuestionAnswer
    {
      get
      {
        return m_iQuestionAnswer;
      }
      set
      {
        m_iQuestionAnswer = value;
      }
    }

    public Enums.QuestionStatus QuestionStatus
    {
      get
      {
        return m_enmQuestionStatus;
      }
      set
      {
        m_enmQuestionStatus = value;
      }
    }
  }
}
