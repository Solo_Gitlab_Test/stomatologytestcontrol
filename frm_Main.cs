﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace StomatologyTestControl
{
  public partial class frm_Main : Form
  {
    private TestQuestionCollection objTestQuestionCollection = new TestQuestionCollection();
    private TestQuestion objCurrentTestQuestion              = null;

    public frm_Main()
    {
      InitializeComponent();
    }

    private void btnStartTest_Click(object sender, EventArgs e)
    {
      if (lstTestParts.SelectedItems == null)
          return;

      if (lstTestParts.SelectedItems.Count == 0)
      {
          Program.ShowErrorMessage("НЕ ВЫБРАНЫ РАЗДЕЛЫ");
          return;
      }

      List<String> lstSelectedItems = new List<String>();

      for (int i = 0; i < lstTestParts.SelectedItems.Count; i++)
        lstSelectedItems.Add(lstTestParts.SelectedItems[i].ToString());

      objTestQuestionCollection.PrepareSelectedQuestions(lstSelectedItems);
      ShowNextQuestion();

      if (chkAnswerTime.Checked)
      {
          tmAnswerTime.Enabled = true;
          tmTime.Enabled       = true;
      }
    }

    private void ShowNextQuestion()
    {
      Int32 iNextQuestionIndex = -1;

      if (rbnDirectly.Checked)
          iNextQuestionIndex = objTestQuestionCollection.GetNextFromSelectedQuestionsDirectly();

      if (rbnRandom.Checked)
          iNextQuestionIndex = objTestQuestionCollection.GetNextFromSelectedQuestionsRandom();
        
      if (iNextQuestionIndex == -1)
      {
          tmAnswerTime.Enabled  = false;
          tmTime.Enabled        = false;
          chkAnswerTime.Checked = false;
          Program.ShowErrorMessage("ОПРОС ПО ВЫБРАННЫМ ВАМИ РАЗДЕЛАМ ЗАКОНЧЕН.");
          return;
      }

      objCurrentTestQuestion = objTestQuestionCollection[iNextQuestionIndex];

      if (objCurrentTestQuestion == null)
          return;

      String strQuestionText = objCurrentTestQuestion.QuestionsPartName +
                               System.Environment.NewLine               +
                               System.Environment.NewLine               +
                               objCurrentTestQuestion.QuestionText;

      txtQuestionText.Text = strQuestionText;

      if (chkShowAnswers.Checked)
          lblAnswer.Text = objCurrentTestQuestion.QuestionAnswer.ToString();
    }

    private void frm_Main_Load(object sender, EventArgs e)
    {
      btnStartTest.Enabled    = false;
      btnNextQuestion.Enabled = false;
      btnAnswer.Enabled       = false;

      objTestQuestionCollection.FillFromFiles();


      lstTestParts.Items.Clear();

      lstTestParts.Items.Add(Program.QuestionsPart01Name);
      lstTestParts.Items.Add(Program.QuestionsPart02Name);
      lstTestParts.Items.Add(Program.QuestionsPart03Name);
      lstTestParts.Items.Add(Program.QuestionsPart04Name);
      lstTestParts.Items.Add(Program.QuestionsPart05Name);
      lstTestParts.Items.Add(Program.QuestionsPart06Name);
      lstTestParts.Items.Add(Program.QuestionsPart07Name);
      lstTestParts.Items.Add(Program.QuestionsPart08Name);
      lstTestParts.Items.Add(Program.QuestionsPart09Name);
      lstTestParts.Items.Add(Program.QuestionsPart10Name);
      lstTestParts.Items.Add(Program.QuestionsPart11Name);
      lstTestParts.Items.Add(Program.QuestionsPart12Name);
      lstTestParts.Items.Add(Program.QuestionsPart13Name);
      lstTestParts.Items.Add(Program.QuestionsPart14Name);
      lstTestParts.Items.Add(Program.QuestionsPart15Name);
      lstTestParts.Items.Add(Program.QuestionsPart16Name);
      lstTestParts.Items.Add(Program.QuestionsPart17Name);

      if (lstTestParts.Items.Count > 0)
          lstTestParts.SelectedIndex = 0;

      for (int i = 1; i <= 10; i++)
        cmbAnswers.Items.Add(i);

      cmbAnswers.SelectedIndex = 0;

      Fill_lstNotCorrectQuestions();

      btnStartTest.Enabled    = true;
      btnNextQuestion.Enabled = true;
      btnAnswer.Enabled       = true;

      lblAnswerTime.Text = ((int)nmrAnswerTime.Value).ToString();
    }

    private void Fill_lstNotCorrectQuestions()
    {
      lstNotCorrectQuestions.Items.Clear();

      if (Directory.Exists("TestQuestionsHistory"))
      {
          String[] arrFiles = Directory.GetFiles("TestQuestionsHistory");

          for (int i = 0; i < arrFiles.Length; i++)
            lstNotCorrectQuestions.Items.Add(arrFiles[i]);

          if (lstNotCorrectQuestions.Items.Count > 0)
              lstNotCorrectQuestions.SelectedIndex = 0;
      } 
    }

    private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
      CheckBox chk = (CheckBox)sender;

      if (chk.Checked)
      {
          for (int i = 0; i < lstTestParts.Items.Count; i++)
            lstTestParts.SetSelected(i, true);
      }

      if (!chk.Checked)
          lstTestParts.ClearSelected();
    }

    private void btnNextQuestion_Click(object sender, EventArgs e)
    {
      if (txtQuestionText.Text == String.Empty)
          return;

      lblAnswer.Text = String.Empty;
      ShowNextQuestion();

      if (chkAnswerTime.Checked)
      {
          tmAnswerTime.Enabled  = false;
          tmTime.Enabled        = false;
          tmAnswerTime.Interval = (int)nmrAnswerTime.Value * 1000;
          lblAnswerTime.Text    = ((int)nmrAnswerTime.Value).ToString();
          tmAnswerTime.Enabled  = true;
          tmTime.Enabled        = true;
      }
    }

    private void btnAnswer_Click(object sender, EventArgs e)
    {
      if (objCurrentTestQuestion.QuestionAnswer == Int32.Parse(cmbAnswers.SelectedItem.ToString()))
      {
          lblAnswer.ForeColor = Color.Lime;

          if (objCurrentTestQuestion.QuestionStatus == Enums.QuestionStatus.Neutral)
              objCurrentTestQuestion.QuestionStatus = Enums.QuestionStatus.Correct;
      }
      else
      {
          lblAnswer.ForeColor = Color.Red;
          objCurrentTestQuestion.QuestionStatus = Enums.QuestionStatus.NotCorrect;
      }

      lblAnswer.Text = objCurrentTestQuestion.QuestionAnswer.ToString();
    }

    private void txtQuestionText_TextChanged(object sender, EventArgs e)
    {
      if (objTestQuestionCollection.SelectedQuestions.Count - 1 >= 0)
          lblRemainQuestionValue.Text = (objTestQuestionCollection.SelectedQuestions.Count - 1).ToString();
      else
          lblRemainQuestionValue.Text = String.Empty;

    }

    private void frm_Main_FormClosed(object sender, FormClosedEventArgs e)
    {
      objTestQuestionCollection.ToTextFile();
    }

    private void btnNotCorrectQuestions_Click(object sender, EventArgs e)
    {
      if (lstNotCorrectQuestions.Items.Count == 0)
          return;

      if (lstNotCorrectQuestions.SelectedItem == null)
          return;

      objTestQuestionCollection.PrepareSelectedQuestions(lstNotCorrectQuestions.SelectedItem.ToString());
      ShowNextQuestion();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      objTestQuestionCollection.ToTextFile();
      Fill_lstNotCorrectQuestions();
    }

    private void chkAnswerTime_CheckedChanged(object sender, EventArgs e)
    {
      CheckBox chk          = (CheckBox)sender;
      tmAnswerTime.Interval = (int)nmrAnswerTime.Value * 1000;
      lblAnswerTime.Text    = ((int)nmrAnswerTime.Value).ToString();

      if (!chk.Enabled)
      {
          tmAnswerTime.Enabled = false;
          tmTime.Enabled       = false;
      }
    }

    private void nmrAnswerTime_ValueChanged(object sender, EventArgs e)
    {
      if (nmrAnswerTime.Value <= 3)
      {
          nmrAnswerTime.Value = 3;
      }

      tmAnswerTime.Interval = (int)nmrAnswerTime.Value * 1000;
      lblAnswerTime.Text    = ((int)nmrAnswerTime.Value).ToString();
      chkAnswerTime.Checked = false;
    }

    private void tmAnswerTime_Tick(object sender, EventArgs e)
    {
      btnNextQuestion_Click(this, new EventArgs());
    }

    private void tmTime_Tick(object sender, EventArgs e)
    {
      Int32 iCurrentValue = Int32.Parse(lblAnswerTime.Text) - 1;

      if (iCurrentValue > 0)
          lblAnswerTime.Text = iCurrentValue.ToString();
      else
          lblAnswerTime.Text = ((int)nmrAnswerTime.Value).ToString();
    }

  }
}
