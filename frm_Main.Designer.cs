﻿namespace StomatologyTestControl
{
  partial class frm_Main
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.grbQuestion = new System.Windows.Forms.GroupBox();
      this.chkAnswerTime = new System.Windows.Forms.CheckBox();
      this.nmrAnswerTime = new System.Windows.Forms.NumericUpDown();
      this.lblAnswerTime = new System.Windows.Forms.Label();
      this.lblRemainQuestionValue = new System.Windows.Forms.Label();
      this.lblRemainQuestionsText = new System.Windows.Forms.Label();
      this.btnNextQuestion = new System.Windows.Forms.Button();
      this.txtQuestionText = new System.Windows.Forms.TextBox();
      this.button1 = new System.Windows.Forms.Button();
      this.chkShowAnswers = new System.Windows.Forms.CheckBox();
      this.btnStartTest = new System.Windows.Forms.Button();
      this.grbAnswer = new System.Windows.Forms.GroupBox();
      this.cmbAnswers = new System.Windows.Forms.ComboBox();
      this.btnAnswer = new System.Windows.Forms.Button();
      this.grbCorrectAnswer = new System.Windows.Forms.GroupBox();
      this.lblAnswer = new System.Windows.Forms.Label();
      this.grbOptions = new System.Windows.Forms.GroupBox();
      this.btnNotCorrectQuestions = new System.Windows.Forms.Button();
      this.lstNotCorrectQuestions = new System.Windows.Forms.ListBox();
      this.chkSelectAll = new System.Windows.Forms.CheckBox();
      this.lstTestParts = new System.Windows.Forms.ListBox();
      this.rbnRandom = new System.Windows.Forms.RadioButton();
      this.rbnDirectly = new System.Windows.Forms.RadioButton();
      this.tmTime = new System.Windows.Forms.Timer(this.components);
      this.tmAnswerTime = new System.Windows.Forms.Timer(this.components);
      this.grbQuestion.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nmrAnswerTime)).BeginInit();
      this.grbAnswer.SuspendLayout();
      this.grbCorrectAnswer.SuspendLayout();
      this.grbOptions.SuspendLayout();
      this.SuspendLayout();
      // 
      // grbQuestion
      // 
      this.grbQuestion.Controls.Add(this.chkAnswerTime);
      this.grbQuestion.Controls.Add(this.nmrAnswerTime);
      this.grbQuestion.Controls.Add(this.lblAnswerTime);
      this.grbQuestion.Controls.Add(this.lblRemainQuestionValue);
      this.grbQuestion.Controls.Add(this.lblRemainQuestionsText);
      this.grbQuestion.Controls.Add(this.btnNextQuestion);
      this.grbQuestion.Controls.Add(this.txtQuestionText);
      this.grbQuestion.Location = new System.Drawing.Point(17, 14);
      this.grbQuestion.Name = "grbQuestion";
      this.grbQuestion.Size = new System.Drawing.Size(561, 517);
      this.grbQuestion.TabIndex = 1;
      this.grbQuestion.TabStop = false;
      this.grbQuestion.Text = "ТЕКУЩИЙ ВОПРОС ТЕСТА";
      // 
      // chkAnswerTime
      // 
      this.chkAnswerTime.AutoSize = true;
      this.chkAnswerTime.Location = new System.Drawing.Point(227, 477);
      this.chkAnswerTime.Name = "chkAnswerTime";
      this.chkAnswerTime.Size = new System.Drawing.Size(15, 14);
      this.chkAnswerTime.TabIndex = 12;
      this.chkAnswerTime.UseVisualStyleBackColor = true;
      this.chkAnswerTime.CheckedChanged += new System.EventHandler(this.chkAnswerTime_CheckedChanged);
      // 
      // nmrAnswerTime
      // 
      this.nmrAnswerTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.nmrAnswerTime.Location = new System.Drawing.Point(248, 477);
      this.nmrAnswerTime.Name = "nmrAnswerTime";
      this.nmrAnswerTime.Size = new System.Drawing.Size(70, 35);
      this.nmrAnswerTime.TabIndex = 13;
      this.nmrAnswerTime.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
      this.nmrAnswerTime.ValueChanged += new System.EventHandler(this.nmrAnswerTime_ValueChanged);
      // 
      // lblAnswerTime
      // 
      this.lblAnswerTime.AutoSize = true;
      this.lblAnswerTime.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblAnswerTime.ForeColor = System.Drawing.Color.Red;
      this.lblAnswerTime.Location = new System.Drawing.Point(331, 477);
      this.lblAnswerTime.Name = "lblAnswerTime";
      this.lblAnswerTime.Size = new System.Drawing.Size(42, 31);
      this.lblAnswerTime.TabIndex = 12;
      this.lblAnswerTime.Text = "10";
      // 
      // lblRemainQuestionValue
      // 
      this.lblRemainQuestionValue.AutoSize = true;
      this.lblRemainQuestionValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblRemainQuestionValue.ForeColor = System.Drawing.Color.Blue;
      this.lblRemainQuestionValue.Location = new System.Drawing.Point(173, 482);
      this.lblRemainQuestionValue.Name = "lblRemainQuestionValue";
      this.lblRemainQuestionValue.Size = new System.Drawing.Size(0, 25);
      this.lblRemainQuestionValue.TabIndex = 11;
      // 
      // lblRemainQuestionsText
      // 
      this.lblRemainQuestionsText.AutoSize = true;
      this.lblRemainQuestionsText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblRemainQuestionsText.Location = new System.Drawing.Point(12, 488);
      this.lblRemainQuestionsText.Name = "lblRemainQuestionsText";
      this.lblRemainQuestionsText.Size = new System.Drawing.Size(154, 13);
      this.lblRemainQuestionsText.TabIndex = 10;
      this.lblRemainQuestionsText.Text = "ОСТАЛОСЬ ВОПРОСОВ: ";
      // 
      // btnNextQuestion
      // 
      this.btnNextQuestion.Location = new System.Drawing.Point(390, 476);
      this.btnNextQuestion.Name = "btnNextQuestion";
      this.btnNextQuestion.Size = new System.Drawing.Size(154, 35);
      this.btnNextQuestion.TabIndex = 5;
      this.btnNextQuestion.Text = "СЛЕДУЮЩИЙ ВОПРОС";
      this.btnNextQuestion.UseVisualStyleBackColor = true;
      this.btnNextQuestion.Click += new System.EventHandler(this.btnNextQuestion_Click);
      // 
      // txtQuestionText
      // 
      this.txtQuestionText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtQuestionText.Location = new System.Drawing.Point(15, 19);
      this.txtQuestionText.Multiline = true;
      this.txtQuestionText.Name = "txtQuestionText";
      this.txtQuestionText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txtQuestionText.Size = new System.Drawing.Size(529, 451);
      this.txtQuestionText.TabIndex = 0;
      this.txtQuestionText.TextChanged += new System.EventHandler(this.txtQuestionText_TextChanged);
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(15, 385);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(168, 35);
      this.button1.TabIndex = 11;
      this.button1.Text = "СОХРАНИТЬ НЕПРАВИЛЬНО ОТВЕЧЕННЫЕ ВОПРОСЫ";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // chkShowAnswers
      // 
      this.chkShowAnswers.AutoSize = true;
      this.chkShowAnswers.Location = new System.Drawing.Point(125, 223);
      this.chkShowAnswers.Name = "chkShowAnswers";
      this.chkShowAnswers.Size = new System.Drawing.Size(150, 17);
      this.chkShowAnswers.TabIndex = 9;
      this.chkShowAnswers.Text = "ПОКАЗЫВАТЬ ОТВЕТЫ";
      this.chkShowAnswers.UseVisualStyleBackColor = true;
      // 
      // btnStartTest
      // 
      this.btnStartTest.Location = new System.Drawing.Point(286, 218);
      this.btnStartTest.Name = "btnStartTest";
      this.btnStartTest.Size = new System.Drawing.Size(122, 46);
      this.btnStartTest.TabIndex = 6;
      this.btnStartTest.Text = "НАЧАТЬ ТЕСТ";
      this.btnStartTest.UseVisualStyleBackColor = true;
      this.btnStartTest.Click += new System.EventHandler(this.btnStartTest_Click);
      // 
      // grbAnswer
      // 
      this.grbAnswer.Controls.Add(this.cmbAnswers);
      this.grbAnswer.Controls.Add(this.btnAnswer);
      this.grbAnswer.Location = new System.Drawing.Point(592, 448);
      this.grbAnswer.Name = "grbAnswer";
      this.grbAnswer.Size = new System.Drawing.Size(208, 83);
      this.grbAnswer.TabIndex = 2;
      this.grbAnswer.TabStop = false;
      this.grbAnswer.Text = "ВЫБЕРИТЕ ОТВЕТ:";
      // 
      // cmbAnswers
      // 
      this.cmbAnswers.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.cmbAnswers.FormattingEnabled = true;
      this.cmbAnswers.Location = new System.Drawing.Point(15, 31);
      this.cmbAnswers.Name = "cmbAnswers";
      this.cmbAnswers.Size = new System.Drawing.Size(83, 32);
      this.cmbAnswers.TabIndex = 5;
      // 
      // btnAnswer
      // 
      this.btnAnswer.Location = new System.Drawing.Point(111, 31);
      this.btnAnswer.Name = "btnAnswer";
      this.btnAnswer.Size = new System.Drawing.Size(91, 32);
      this.btnAnswer.TabIndex = 4;
      this.btnAnswer.Text = "ОТВЕТИТЬ";
      this.btnAnswer.UseVisualStyleBackColor = true;
      this.btnAnswer.Click += new System.EventHandler(this.btnAnswer_Click);
      // 
      // grbCorrectAnswer
      // 
      this.grbCorrectAnswer.Controls.Add(this.lblAnswer);
      this.grbCorrectAnswer.Location = new System.Drawing.Point(806, 448);
      this.grbCorrectAnswer.Name = "grbCorrectAnswer";
      this.grbCorrectAnswer.Size = new System.Drawing.Size(200, 83);
      this.grbCorrectAnswer.TabIndex = 5;
      this.grbCorrectAnswer.TabStop = false;
      this.grbCorrectAnswer.Text = "ПРАВИЛЬНЫЙ ОТВЕТ";
      // 
      // lblAnswer
      // 
      this.lblAnswer.AutoSize = true;
      this.lblAnswer.Font = new System.Drawing.Font("Times New Roman", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblAnswer.Location = new System.Drawing.Point(75, 16);
      this.lblAnswer.Name = "lblAnswer";
      this.lblAnswer.Size = new System.Drawing.Size(0, 61);
      this.lblAnswer.TabIndex = 0;
      // 
      // grbOptions
      // 
      this.grbOptions.Controls.Add(this.button1);
      this.grbOptions.Controls.Add(this.btnNotCorrectQuestions);
      this.grbOptions.Controls.Add(this.chkShowAnswers);
      this.grbOptions.Controls.Add(this.lstNotCorrectQuestions);
      this.grbOptions.Controls.Add(this.chkSelectAll);
      this.grbOptions.Controls.Add(this.lstTestParts);
      this.grbOptions.Controls.Add(this.btnStartTest);
      this.grbOptions.Controls.Add(this.rbnRandom);
      this.grbOptions.Controls.Add(this.rbnDirectly);
      this.grbOptions.Location = new System.Drawing.Point(592, 14);
      this.grbOptions.Name = "grbOptions";
      this.grbOptions.Size = new System.Drawing.Size(414, 428);
      this.grbOptions.TabIndex = 6;
      this.grbOptions.TabStop = false;
      this.grbOptions.Text = "НАСТРОЙКИ (ВЫБЕРИТЕ РАЗДЕЛ(Ы) И ПОРЯДОК ВЫБОРКИ)";
      // 
      // btnNotCorrectQuestions
      // 
      this.btnNotCorrectQuestions.Location = new System.Drawing.Point(214, 385);
      this.btnNotCorrectQuestions.Name = "btnNotCorrectQuestions";
      this.btnNotCorrectQuestions.Size = new System.Drawing.Size(194, 35);
      this.btnNotCorrectQuestions.TabIndex = 10;
      this.btnNotCorrectQuestions.Text = "ПРОЙТИСЬ ПО ОТВЕЧЕННЫМ НЕПРАВИЛЬНО";
      this.btnNotCorrectQuestions.UseVisualStyleBackColor = true;
      this.btnNotCorrectQuestions.Click += new System.EventHandler(this.btnNotCorrectQuestions_Click);
      // 
      // lstNotCorrectQuestions
      // 
      this.lstNotCorrectQuestions.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lstNotCorrectQuestions.FormattingEnabled = true;
      this.lstNotCorrectQuestions.HorizontalScrollbar = true;
      this.lstNotCorrectQuestions.ItemHeight = 21;
      this.lstNotCorrectQuestions.Location = new System.Drawing.Point(15, 270);
      this.lstNotCorrectQuestions.Name = "lstNotCorrectQuestions";
      this.lstNotCorrectQuestions.ScrollAlwaysVisible = true;
      this.lstNotCorrectQuestions.Size = new System.Drawing.Size(393, 109);
      this.lstNotCorrectQuestions.TabIndex = 9;
      // 
      // chkSelectAll
      // 
      this.chkSelectAll.AutoSize = true;
      this.chkSelectAll.Location = new System.Drawing.Point(15, 223);
      this.chkSelectAll.Name = "chkSelectAll";
      this.chkSelectAll.Size = new System.Drawing.Size(102, 17);
      this.chkSelectAll.TabIndex = 8;
      this.chkSelectAll.Text = "ВЫБРАТЬ ВСЕ";
      this.chkSelectAll.UseVisualStyleBackColor = true;
      this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
      // 
      // lstTestParts
      // 
      this.lstTestParts.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lstTestParts.FormattingEnabled = true;
      this.lstTestParts.HorizontalScrollbar = true;
      this.lstTestParts.ItemHeight = 21;
      this.lstTestParts.Location = new System.Drawing.Point(15, 20);
      this.lstTestParts.Name = "lstTestParts";
      this.lstTestParts.ScrollAlwaysVisible = true;
      this.lstTestParts.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
      this.lstTestParts.Size = new System.Drawing.Size(393, 193);
      this.lstTestParts.TabIndex = 7;
      // 
      // rbnRandom
      // 
      this.rbnRandom.AutoSize = true;
      this.rbnRandom.Location = new System.Drawing.Point(125, 243);
      this.rbnRandom.Name = "rbnRandom";
      this.rbnRandom.Size = new System.Drawing.Size(155, 17);
      this.rbnRandom.TabIndex = 1;
      this.rbnRandom.Text = "СЛУЧАЙНЫЙ ПОРЯДОК";
      this.rbnRandom.UseVisualStyleBackColor = true;
      // 
      // rbnDirectly
      // 
      this.rbnDirectly.AutoSize = true;
      this.rbnDirectly.Checked = true;
      this.rbnDirectly.Location = new System.Drawing.Point(15, 243);
      this.rbnDirectly.Name = "rbnDirectly";
      this.rbnDirectly.Size = new System.Drawing.Size(99, 17);
      this.rbnDirectly.TabIndex = 0;
      this.rbnDirectly.TabStop = true;
      this.rbnDirectly.Text = "ПО ПОРЯДКУ";
      this.rbnDirectly.UseVisualStyleBackColor = true;
      // 
      // tmTime
      // 
      this.tmTime.Interval = 1000;
      this.tmTime.Tick += new System.EventHandler(this.tmTime_Tick);
      // 
      // tmAnswerTime
      // 
      this.tmAnswerTime.Tick += new System.EventHandler(this.tmAnswerTime_Tick);
      // 
      // frm_Main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1057, 543);
      this.Controls.Add(this.grbOptions);
      this.Controls.Add(this.grbCorrectAnswer);
      this.Controls.Add(this.grbAnswer);
      this.Controls.Add(this.grbQuestion);
      this.Name = "frm_Main";
      this.Text = "Проверка знания вопросов теста по стоматологии";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_Main_FormClosed);
      this.Load += new System.EventHandler(this.frm_Main_Load);
      this.grbQuestion.ResumeLayout(false);
      this.grbQuestion.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nmrAnswerTime)).EndInit();
      this.grbAnswer.ResumeLayout(false);
      this.grbCorrectAnswer.ResumeLayout(false);
      this.grbCorrectAnswer.PerformLayout();
      this.grbOptions.ResumeLayout(false);
      this.grbOptions.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox grbQuestion;
    private System.Windows.Forms.TextBox txtQuestionText;
    private System.Windows.Forms.GroupBox grbAnswer;
    private System.Windows.Forms.Button btnAnswer;
    private System.Windows.Forms.GroupBox grbCorrectAnswer;
    private System.Windows.Forms.Button btnNextQuestion;
    private System.Windows.Forms.GroupBox grbOptions;
    private System.Windows.Forms.RadioButton rbnRandom;
    private System.Windows.Forms.RadioButton rbnDirectly;
    private System.Windows.Forms.Button btnStartTest;
    private System.Windows.Forms.ListBox lstTestParts;
    private System.Windows.Forms.CheckBox chkSelectAll;
    private System.Windows.Forms.CheckBox chkShowAnswers;
    private System.Windows.Forms.ComboBox cmbAnswers;
    private System.Windows.Forms.Label lblAnswer;
    private System.Windows.Forms.Label lblRemainQuestionValue;
    private System.Windows.Forms.Label lblRemainQuestionsText;
    private System.Windows.Forms.Button btnNotCorrectQuestions;
    private System.Windows.Forms.ListBox lstNotCorrectQuestions;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Label lblAnswerTime;
    private System.Windows.Forms.NumericUpDown nmrAnswerTime;
    private System.Windows.Forms.CheckBox chkAnswerTime;
    private System.Windows.Forms.Timer tmTime;
    private System.Windows.Forms.Timer tmAnswerTime;
  }
}

