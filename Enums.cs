using System;
using System.Collections.Generic;
using System.Text;

namespace StomatologyTestControl
{
  public static class Enums
  {
    public enum QuestionStatus: int
    {
      Correct    = 1,
      NotCorrect = 2,
      Neutral    = 3
    };

  }
}
