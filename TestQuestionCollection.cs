﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Collections;
using System.IO;

namespace StomatologyTestControl
{
  public class TestQuestionCollection : System.Collections.CollectionBase
  {
    private List<Int32> m_lstSelectedQuestions         = new List<Int32>();
    private Int32       m_iCurrentQuestionIndex        = -1;
    private Boolean     m_blnIsSaveNotCorrectQuestions = true;

    public void ToTextFile()
    {
      if (!m_blnIsSaveNotCorrectQuestions)
          return;

      if (!this.IfNotCorrectQuestionsExists)
          return;

      if (!Directory.Exists("TestQuestionsHistory"))
          Directory.CreateDirectory("TestQuestionsHistory");

      using (StreamWriter sw = File.CreateText(@"TestQuestionsHistory\TestQuestionsHistory__" + DateTime.Now.ToString().Replace(':', '_') + ".txt"))
      {
        for (int i = 0; i < this.Count; i++)
        {
          if (this[i].QuestionStatus == Enums.QuestionStatus.NotCorrect)
          {
              String str = i.ToString() + "#" + this[i].QuestionsPartName + "#" + this[i].QuestionNumber;
              sw.WriteLine(str);
          }
        }
      }
    }

    public void PrepareSelectedQuestions(List<String> lstParts)
    {
      m_lstSelectedQuestions.Clear();
      m_iCurrentQuestionIndex = -1;

      for (int i = 0; i < lstParts.Count; i++)
      {
        for (int j = 0; j < this.Count; j++)
        {
          if (lstParts[i] == this[j].QuestionsPartName)
          {
              m_lstSelectedQuestions.Add(j);
          }
        }
      }

      m_blnIsSaveNotCorrectQuestions = true;
    }

    public void PrepareSelectedQuestions(String strFileName)
    {
      if (!File.Exists(strFileName))
          return;

      m_lstSelectedQuestions.Clear();
      m_iCurrentQuestionIndex = -1;

      using (StreamReader sr = File.OpenText(strFileName))
      {
        String str = "";

        while ((str = sr.ReadLine()) != null)
        {
          Int32 j = Int32.Parse(str.Split('#')[0]);
          m_lstSelectedQuestions.Add(j);
        }
      }

      m_blnIsSaveNotCorrectQuestions = false;
    }

    public Int32 GetNextFromSelectedQuestionsDirectly()
    {
      Int32 iReturnValue = -1;

      if (m_iCurrentQuestionIndex != -1)
          m_lstSelectedQuestions.Remove(m_iCurrentQuestionIndex);

      if (m_lstSelectedQuestions.Count > 0)
          iReturnValue = m_lstSelectedQuestions[0];

      m_iCurrentQuestionIndex = iReturnValue;

      return iReturnValue;
    }

    public Int32 GetNextFromSelectedQuestionsRandom()
    {
      Int32 iReturnValue = -1;

      if (m_iCurrentQuestionIndex != -1)
          m_lstSelectedQuestions.Remove(m_iCurrentQuestionIndex);

      if (m_lstSelectedQuestions.Count > 0)
      {
          Random rand  = new Random();
          iReturnValue = m_lstSelectedQuestions[rand.Next(m_lstSelectedQuestions.Count)];
      }

      m_iCurrentQuestionIndex = iReturnValue;

      return iReturnValue;
    }

    public void FillFromFiles()
    {
      FillFromFile(Program.QuestionsPart01FileName, Program.AnswersPart01FileName,
                   Program.QuestionsPart01Name, Program.QuestionsPart01ControlSum);

      FillFromFile(Program.QuestionsPart02FileName, Program.AnswersPart02FileName,
                   Program.QuestionsPart02Name, Program.QuestionsPart02ControlSum);

      FillFromFile(Program.QuestionsPart03FileName, Program.AnswersPart03FileName,
                   Program.QuestionsPart03Name, Program.QuestionsPart03ControlSum);

      FillFromFile(Program.QuestionsPart04FileName, Program.AnswersPart04FileName,
                   Program.QuestionsPart04Name, Program.QuestionsPart04ControlSum);

      FillFromFile(Program.QuestionsPart05FileName, Program.AnswersPart05FileName,
                   Program.QuestionsPart05Name, Program.QuestionsPart05ControlSum);

      FillFromFile(Program.QuestionsPart06FileName, Program.AnswersPart06FileName,
                   Program.QuestionsPart06Name, Program.QuestionsPart06ControlSum);

      FillFromFile(Program.QuestionsPart07FileName, Program.AnswersPart07FileName,
                   Program.QuestionsPart07Name, Program.QuestionsPart07ControlSum);

      FillFromFile(Program.QuestionsPart08FileName, Program.AnswersPart08FileName,
                   Program.QuestionsPart08Name, Program.QuestionsPart08ControlSum);

      FillFromFile(Program.QuestionsPart09FileName, Program.AnswersPart09FileName,
                   Program.QuestionsPart09Name, Program.QuestionsPart09ControlSum);

      FillFromFile(Program.QuestionsPart10FileName, Program.AnswersPart10FileName,
                   Program.QuestionsPart10Name, Program.QuestionsPart10ControlSum);

      FillFromFile(Program.QuestionsPart11FileName, Program.AnswersPart11FileName,
                   Program.QuestionsPart11Name, Program.QuestionsPart11ControlSum);

      FillFromFile(Program.QuestionsPart12FileName, Program.AnswersPart12FileName,
                   Program.QuestionsPart12Name, Program.QuestionsPart12ControlSum);

      FillFromFile(Program.QuestionsPart13FileName, Program.AnswersPart13FileName,
                   Program.QuestionsPart13Name, Program.QuestionsPart13ControlSum);

      FillFromFile(Program.QuestionsPart14FileName, Program.AnswersPart14FileName,
                   Program.QuestionsPart14Name, Program.QuestionsPart14ControlSum);

      FillFromFile(Program.QuestionsPart15FileName, Program.AnswersPart15FileName,
                   Program.QuestionsPart15Name, Program.QuestionsPart15ControlSum);

      FillFromFile(Program.QuestionsPart16FileName, Program.AnswersPart16FileName,
                   Program.QuestionsPart16Name, Program.QuestionsPart16ControlSum);

      FillFromFile(Program.QuestionsPart17FileName, Program.AnswersPart17FileName,
                   Program.QuestionsPart17Name, Program.QuestionsPart17ControlSum);
    }

    public void FillFromFile(String strQuestionsFileName, String strAnswersFileName, 
                             String strQuestionsPartName, Int32 iControlSum)
    {
      if (!File.Exists(strQuestionsFileName) || !File.Exists(strAnswersFileName))
          return;

      using (StreamReader srQuestions = File.OpenText(strQuestionsFileName))
      {
        String str            = srQuestions.ReadToEnd();
        String[] arrQuestions = str.Split('#');

        if (arrQuestions.Length-1 != iControlSum)
        {
            Program.ShowErrorMessage("Неверная контрольная сумма");
            return;
        }

        using (StreamReader srAnswers = File.OpenText(strAnswersFileName))
        {
          String strAnswerText = String.Empty;

          for (int i = 1; i < arrQuestions.Length; i++)
          {
            strAnswerText         = srAnswers.ReadLine();

            Int32 iAnswerNumber   = Int32.Parse(strAnswerText.Split('.')[0]);
            Int32 iAnswer         = Int32.Parse(strAnswerText.Split('.')[1]);
            Int32 iQuestionNumber = Int32.Parse(arrQuestions[i].Split('.')[0]);

            if (iQuestionNumber != iAnswerNumber)
            {
                Program.ShowErrorMessage("Номер вопроса не соответствует номеру ответа");
                return;
            }

            TestQuestion objTestQuestion      = new TestQuestion();

            objTestQuestion.QuestionNumber    = iQuestionNumber;
            objTestQuestion.QuestionText      = arrQuestions[i];
            objTestQuestion.QuestionsPartName = strQuestionsPartName;
            objTestQuestion.QuestionStatus    = Enums.QuestionStatus.Neutral;
            objTestQuestion.QuestionAnswer    = iAnswer;

            this.Add(objTestQuestion);
          }
        }
      }
    }

    public Int32 Add(TestQuestion objTestQuestion)
    {
      return (List.Add(objTestQuestion));
    }

    public Int32 IndexOf(TestQuestion objTestQuestion)
    {
      return (List.IndexOf(objTestQuestion));
    }

    public void Insert(Int32 index, TestQuestion objTestQuestion)
    {
      List.Insert(index, objTestQuestion);
    }

    public void Remove(TestQuestion objTestQuestion)
    {
      List.Remove(objTestQuestion);
    }

    public TestQuestion this[int index]
    {
      get
      {
        return (List[index] as TestQuestion);
      }
      set
      {
        List[index] = value;
      }
    }

    public List<Int32> SelectedQuestions
    {
      get
      {
        return m_lstSelectedQuestions;
      }
    }

    public Boolean IfNotCorrectQuestionsExists
    {
      get
      {
        for (int i = 0; i < this.Count; i++)
        {
          if (this[i].QuestionStatus == Enums.QuestionStatus.NotCorrect)
              return true;
        }

        return false;
      }
    }

  }
}
